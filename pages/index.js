import Head from "next/head";
import styles from "../styles/Home.module.css";
import "bulma/css/bulma.css";
import Web3 from "web3";
import { useEffect, useState } from "react";
import lotteryContract from "../blockchain/lottery";
export default function Home() {
  const [web3, setWeb3] = useState();
  const [address, setAddress] = useState();
  const [contract, setContract] = useState();
  const [potBalance, setPotBalance] = useState();
  const [players, setPlayers] = useState([]);
  const [error, setError] = useState("");
  const [successMessage, setSuccessMessage] = useState("");
  const [lotteryId, setLotteryId] = useState();
  const [lotteryHistory, setLotteryHistory] = useState([]);

  useEffect(() => {
    updateData();
    const id = setInterval(updateData, 30000);
    return () => clearInterval(id);
  }, [contract]);

  const updateData = () => {
    console.log("reloading data");
    if (contract) {
      getPotBalance();
      getPlayers();
      getLotteryId();
    }
  };

  const getPotBalance = async () => {
    const pot = await contract.methods.getBalance().call();
    setPotBalance(web3.utils.fromWei(pot, "ether"));
  };

  const getPlayers = async () => {
    const pot = await contract.methods.getPlayers().call();
    setPlayers(pot);
  };

  const enterLottery = async () => {
    setSuccessMessage("");
    setError("");
    try {
      await contract.methods.enterPot().send({
        from: address,
        value: web3.utils.toWei("0.01", "ether"),
        gas: 300000,
        gasPrice: null,
      });
      setSuccessMessage("Entered this round!");
      setError("");
      updateData();
    } catch (err) {
      setError(err.message);
    }
  };

  const pickWinner = async () => {
    setSuccessMessage("");
    setError("");
    try {
      await contract.methods.pickWinner().send({
        from: address,
        gas: 10000000,
      });
      setSuccessMessage("Successfully picked a winner!");
      setError("");
    } catch (err) {
      setError(err.message);
    }
  };

  const getHistory = async (id) => {
    setLotteryHistory([]);
    const newHistory = [];
    for (let i = parseInt(id) - 1; i > 0; i--) {
      const winnerAddress = await contract.methods.getWinner(i).call();
      const historyObj = {};
      historyObj.id = i;
      historyObj.address = winnerAddress;
      newHistory.push(historyObj);
    }
    setLotteryHistory(newHistory);
  };

  const getLotteryId = async () => {
    const id = await contract.methods.getCurrentLotteryId().call();
    setLotteryId(id);
    await getHistory(id);
  };

  const connectWalletHandler = async () => {
    if (
      typeof window !== "undefined" &&
      typeof window.ethereum !== "undefined"
    ) {
      try {
        await window.ethereum.request({ method: "eth_requestAccounts" });
        const web3 = new Web3(window.ethereum);
        setWeb3(web3);

        const accounts = await web3.eth.getAccounts();
        setAddress(accounts[0]);
        const contract = lotteryContract(web3);
        setContract(contract);
        setError("");
        setSuccessMessage("Connected successfully");
        updateData();
      } catch (err) {
        setError(err.message);
      }
    } else {
      setError("Please install metamask");
    }
  };

  const disconnectWalletHandler = async () => {
    const walletAddress = await window.ethereum.request({
      method: "eth_requestAccounts",
      params: [
        {
          eth_accounts: {},
        },
      ],
    });
    setAddress(null);
  };

  return (
    <div>
      <Head>
        <title>Lottery Dapp</title>
        <meta
          name="description"
          content="Lottery Dapp On Ethereum Rinkeby Testnet"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <nav className="navbar my-4">
          <div className="container">
            <div className="navbar-brand">
              <h1>Lottery Dapp</h1>
            </div>
            <div className="navbar-end">
              <button
                onClick={
                  !address ? connectWalletHandler : disconnectWalletHandler
                }
                className="button is-info mt-2"
              >
                {!address ? "Connect wallet" : address}
              </button>
            </div>
          </div>
        </nav>
        <div className="container">
          <section className="mt-5">
            <div className="columns">
              <div className="column is-two-thirds">
                <section className="mt-5">
                  <p>Enter the lottery by sending 0.01 ether</p>
                  <button
                    onClick={enterLottery}
                    className="button is-link is-large is-light mt-3"
                  >
                    Join this round
                  </button>
                </section>
                <section className="mt-6">
                  <p>
                    <b>Game master only:</b> Randomly pick a winner
                  </p>
                  <button
                    onClick={pickWinner}
                    className="button is-primary is-large is-light mt-3"
                  >
                    Roll the dice
                  </button>
                </section>
                {error && (
                  <section className="mt-6">
                    <div className="container has-text-danger ">{error}</div>
                  </section>
                )}
                {successMessage && (
                  <section className="mt-6">
                    <div className="container has-text-success ">
                      {successMessage}
                    </div>
                  </section>
                )}
              </div>
              <div className={`${styles.lotteryinfo} column is-one-third`}>
                <section className="mt-5">
                  <div className="card">
                    <div className="card-content">
                      <div className="content">
                        <h2>Lottery History</h2>
                        {lotteryHistory &&
                          lotteryHistory.length > 0 &&
                          lotteryHistory.map((item, index) => {
                            return (
                              <div
                                key={`${item.address}-history-${index}`}
                                className="history-entries"
                              >
                                <div>Lottery pot #{item.id} winner: </div>
                                <div>
                                  <a
                                    href={`https://etherscan.io/address/${item.address}`}
                                    target="_blank"
                                    rel="noreferrer"
                                  >
                                    {item.address}
                                  </a>
                                </div>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                  </div>
                </section>
                <section className="mt-5">
                  <div className="card">
                    <div className="card-content">
                      <div className="content">
                        <h2>Players ({players.length}): </h2>
                        <div className="players-list">
                          <div>
                            {players &&
                              players.length > 0 &&
                              players.map((player, index) => {
                                return (
                                  <div key={`${player}-${index}`}>
                                    <a
                                      href={`https://etherscan.io/address/${player}`}
                                      target="_blank"
                                      rel="noreferrer"
                                    >
                                      {player}
                                    </a>
                                  </div>
                                );
                              })}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section className="mt-5">
                  <div className="card">
                    <div className="card-content">
                      <div className="content">
                        <h2>Pot balance</h2>
                        <p>{potBalance} ethers</p>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </section>
        </div>
      </main>

      <footer className={styles.footer}>
        <p>&copy; 2022 Lottery Dapp By The Boys</p>
      </footer>
    </div>
  );
}
