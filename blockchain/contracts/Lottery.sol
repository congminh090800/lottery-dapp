// SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import "@chainlink/contracts/src/v0.8/interfaces/VRFCoordinatorV2Interface.sol";
import "@chainlink/contracts/src/v0.8/VRFConsumerBaseV2.sol";

contract Lottery is VRFConsumerBaseV2 {
    address public owner;
    address payable[] public players; 
    uint lotteryId;
    mapping (uint => address payable) public previousWinners;
    uint previousRandomNumber = 0;
    
    // rinkeby only
    VRFCoordinatorV2Interface COORDINATOR;
    uint64 s_subscriptionId;
    address vrfCoordinator = 0x6168499c0cFfCaCD319c818142124B7A15E857ab;
    bytes32 keyHash = 0xd89b2bf150e3b9e13446986e571fb9cab24b13cea0a43ea20a6049a85cc807cc;
    uint32 callbackGasLimit = 100000;
    uint16 requestConfirmations = 3;
    uint32 numWords =  1;

    uint256 public s_randomWord;
    uint256 public s_requestId;

    constructor(uint64 subscriptionId) VRFConsumerBaseV2(vrfCoordinator) {
        require(subscriptionId > 0);
        
        COORDINATOR = VRFCoordinatorV2Interface(vrfCoordinator);
        s_subscriptionId = subscriptionId;

        owner = msg.sender;
        lotteryId = 1;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
  
    function fulfillRandomWords(
        uint256, /* requestId */
        uint256[] memory randomWords
    ) internal override {
        s_randomWord = randomWords[0];
        uint index = s_randomWord % players.length;
        players[index].transfer(address(this).balance * 3 / 4);

        previousWinners[lotteryId] = players[index];
        lotteryId++;
        previousRandomNumber = s_randomWord;
        players = new address payable[](0);
    }

    function pickWinner() public onlyOwner {
        s_requestId = COORDINATOR.requestRandomWords(
            keyHash,
            s_subscriptionId,
            requestConfirmations,
            callbackGasLimit,
            numWords
        );
    }

    function enterPot() public payable {
        require(msg.value >= 0.0001 ether);
        players.push(payable(msg.sender));
    }

    function getCurrentLotteryId() public view returns (uint) {
        return lotteryId;
    }

    function getBalance() public view returns (uint) {
        return address(this).balance;
    }

    function getPlayers() public view returns (address payable[] memory) {
        return players;
    }

    function getWinner(uint lottery) public view returns (address payable) {
        return previousWinners[lottery]; 
    }
}